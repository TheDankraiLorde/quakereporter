import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';

void main() async {
  Map _obj = await _getJSON();
  List _feat = _obj['features'];
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    home: Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text("Quakes"),
        backgroundColor: Colors.blueGrey,
      ),
      body: ListView.builder(
        itemCount: _feat.length,
        padding: EdgeInsets.all(5.0),
        itemBuilder: (BuildContext context, int index) {
          return Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              ListTile(
                title: Text(
                  _getDate(_feat[index]["properties"]["time"]),
                  style: TextStyle(color: Colors.orangeAccent, fontSize: 20),
                ),
                subtitle: Text(
                    _feat[index]["properties"]["place"],
                      style:TextStyle(color: Colors.white)),
                leading: CircleAvatar(
                  child: Text(_feat[index]["properties"]["mag"].toString()),
                ),
                onTap: () => _show(context, _feat[index]["properties"]),
              ),
              Divider(
                height: 3.4,
                indent: 3.5,
              ),
            ],
          );
        },
      ),
      backgroundColor: Colors.black,
    ),
  ));
}

Future<Map> _getJSON() async {
  String apiURL =
      "https://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/all_day.geojson";
  http.Response rsp = await http.get(apiURL);
  return json.decode(rsp.body);
}

String _getDate(int mill) {
  DateTime dt = DateTime.fromMillisecondsSinceEpoch(mill);
  String df = DateFormat('MMM d, yyyy hh:mm a').format(dt).toString();
  return df;
}

String displayFormatter(double mag, String loc) {
  return "Magnitude: ${mag.toStringAsFixed(1)}\nLocation: $loc";
}

void _show(BuildContext context, Map<String, dynamic> prop) {
  var boop = AlertDialog(
    title: Text("Quake Alert!",
        style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
    content: Text(displayFormatter(prop["mag"], prop["place"]),
        textAlign: TextAlign.center),
    actions: <Widget>[
      FlatButton(
        child: Text("OK"),
        onPressed: () {
          return Navigator.of(context).pop();
        },
      )
    ],
  );

  showDialog(context: context, builder: (context) => boop);
}
